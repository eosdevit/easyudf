/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2017 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "easyudf.h"

int main()
{
  char buf[1024];

  int i, ok;

  double d;

  const char *bban[64] =
  {
     "5390", "14000",        "91363", "Q0539014000000000091363",
    "06055", "00000", "000000000000", "I0605500000000000000000",
          0, "67890", "000000000000",                        "",
    "12345",       0, "000000000000",                        "",
    "12345", "67890",              0,                        "",

    "FINE"
  };

  const char *cf_cognome[64] =
  {
    "BIANCHI ROSSI", "BNC",
     "ABBATANTUONO", "BBT",
     "DE CRESCENZO", "DCR",
         "ROSSANDA", "RSS",
           "MORINI", "MRN",
			"ROSSI", "RSS",
             "ALO'", "LAO",
             "POLI", "PLO",
             "RIVA", "RVI",
               "RE", "REX",
           0, "XXX",

    "FINE"
  };

  const char *cf_cc[64] =
  {
    "BBTDMN86H29G713", "P",
    "MRNMNL73L28G702", "P",
     "XXXXXX00X00X00", "?",
                    0, "?",

    "FINE"
  };

  const char *cf_check[64] =
  {
    "ABCDEF12G34H567I", "0",
    "BBTDMN86H29G713P", "1",
    "BNNLEA15R46G702B", "0",
    "BNNLEA15R46G702J", "1",
    "CMBBDA31A66E225B", "1",
    "DLCGNL42E47E625F", "1",
    "FTTRNT27E04C6090", "0",
    "GSPRLA54S67G822L", "1",
    "MRNMNL73L28G702P", "1",
    "MRNMNL73L28G702Q", "0",
    "PPPPLT80R10M082K", "1",
    "RSSBBR69C48F839A", "1",
                     0, "0",
    "FINE"
  };

  const char *cf_nascita[64] =
  {
    "BBTDMN86H29G713P", "86", "06", "29",
    "GGNMRC90B02E801I", "90", "02", "02",
    "MRNMNL73L28G702P", "73", "07", "28",
                     0, "00", "13", "00",

    "FINE"
  };

  const char *cf_nome[64] =
  {
    "MARIANGELA", "MNG",
    "ALDO MARIA", "LMR",
	 "MARIA PIA", "MRP",
       "DAMIANO", "DMN",
        "MANLIO", "MNL",
         "BRUNO", "BRN",
         "MARTA", "MRT",
          "LUCA", "LCU",
          "SALA", "SLA",
           "LIA", "LIA",
            "AL", "LAX",
            "LI", "LIX",
               0, "XXX",

    "FINE"
  };

  const char *cf_sesso[64] =
  {
    "BBTDMN86H29G713P", "M",
    "MRNMNL73L28G702P", "M",
    "RSSBBR69C48F839A", "F",
                     0, "?",

    "FINE"
  };

  const char *cin[64] =
  {
    "0539014000000000091363", "Q",
    "0605500000000000000000", "I",
                           0, "?",

    "FINE"
  };

  const char *email[64] =
  {
     "dummy@domain.it", "1",
      "dummy@domain.i", "0",
    "dum!my@domain.it", "0",
                     0, "0",

    "FINE"
  };

  const char *floatstr[64] =
  {
     "6.012",   "%.2f",  "6.01",
      "7.34",   "%.1f",   "7.3",
       "6.1", "%.1f%%",  "6.1%",
       "6.1",   "%.0e", "6e+00",
           0,   "%.2f",      "",

	"FINE"
  };

  const char *gcd[64] =
  {
     "9", "27",  "9",
    "13",  "0", "13",
    "12", "18",  "6",
       0, "13",  "0",
    "13",    0,  "0",

    "FINE"
  };

  const char *gtin[64] =
  {
         "1017422567805", "1",
         "1017422567803", "0",
        "12345678901231", "1",
    "123456789012345613", "1",
                       0, "0",

    "FINE"
  };

  const char *iban[64] =
  {
    "IT", "S0539014000000000000004", "IT36S0539014000000000000004",
    "IT", "Q0539014000000000091363", "IT45Q0539014000000000091363",
    "AT",        "1904300234573201",        "AT611904300234573201",
       0,        "1904300234573201",                            "",
    "IT",                         0,                            "",

    "FINE"
  };

  const char *ip4[64] =
  {
        "0.0.0.0", "1",
    "192.168.1.1", "1",
    "192.999.1.1", "0",
         "192.12", "0",
     "1111.1.1.1", "0",
      "256.1.2.3", "0",
       "10.0.0.2", "1",
      "120.A.0.1", "0",
                0, "0",

    "FINE"
  };

  const char *lcm[64] =
  {
     "9", "27", "27",
    "13",  "0",  "0",
       0, "13",  "0",
    "13",    0,  "0",

    "FINE"
  };

  const char *ld[64] =
  {
                  "sport",                 "spot",  "1",
    "appropriate meaning", "approximate matching",  "7",
                        0,                "dummy", "-1",
                  "dummy",                      0, "-1",

    "FINE"
  };

  const char *luhn[64] =
  {
    "456565654", "1",
              0, "0",

    "FINE"
  };

  const char *np[64] =
  {
    "", "", "1",
    "MARCO TOGNI", "TOGNI MARCO", "1",
    "MANLIO MORINI", "MANLIO MORSINI", "0",
    "D'ORLEANS ANDRE", "ANDRE D ORLEANS", "1",
    "ANDREA ROSSI", "MARCO MARCHI", "0",
    "ANDREA ROSSI", "ROSSI ANDREA.", "1",
    "FRITZ VON WILLEBRAND", "VON WILLEBRAND FRITZ", "1",
    "CICCIO-CIOCCI", "CIOCCI-CICCIO", "1",
    "'ARTIERI STEFANO'", "STEFANO ARTIERI", "1",
    "SABELLI LUCIO", "SABELLI LUCA", "0",
    "....", "PISA", "0",
    ".....", "", "1",
    ".....", "  ", "1",
    "MARCO MARCHI", "                   ", "0",
    "MARCO MARCHI MARCHETTI MARCUCCI", "MARCUCCI MARCO MARCHETTI MARCHI", "1",

    "FINE"
  };

  const char *piva[64] =
  {
    "00000000000", "1",
    "01644560508", "1",
         "123456", "0",
                0, "0",

    "FINE"
  };

  const char *sha256[128] =
  {
    "abc", "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad",
    "The quick brown fox jumps over the lazy dog", "d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592",
    "The quick brown fox jumps over the lazy cog", "e4c4d8f3bf76b692de791a173e05321150f7a345b46484fe427f6acc7ecc81be",
    "", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
    0, "",

    "FINE"
  };

  const char *sia[64] =
  {
    "1234A", "1",
    "5A123", "0",
		  0, "0",

    "FINE"
  };

  const char *soundex[64] =
  {
    "Knightridder", "K523",
        "Ashcroft", "A261",
       "Pflanders", "P453",
                 0,     "",

    "FINE"
  };

  const double lat_london = 51.5, lon_london = -0.1167;
  const double lat_pisa = 43.7167, lon_pisa = 10.3833;
  const double lat_tokyo = 35.68, lon_tokyo = 139.74;

  printf("\n\nINIZIO TEST\n");
  printf("----------------------------------------\n");

  /*
   *  BBAN
   */
  ok = 1;
  printf("Verifica fn_bban:");

  for (i = 0; !bban[i] || strcmp(bban[i],"FINE") ; i+=4)
  {
    fn_bban(bban[i],bban[i+1],bban[i+2],buf);

    if (strcmp(buf,bban[i+3]))
    {
      ok = 0;
      printf("\n  ERRORE BBAN('%s','%s','%s')\n  Corretto  %s\n  Calcolato %s\n",bban[i],bban[i+1],bban[i+2],bban[i+3],buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CF_CHECK
   */
  ok = 1;
  printf("Verifica fn_cf_check:");

  for (i = 0; !cf_check[i] || strcmp(cf_check[i],"FINE") ; i+=2)
  {
    const int risposta = fn_cf_check(cf_check[i]);
    const int giusto = cf_check[i+1][0]==(char)'0' ? 0 : 1;
	if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE CF_CHECK('%s')\n  Corretto %d\n  Calcolato %d\n",cf_check[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CF_COGNOME
   */
  ok = 1;
  printf("Verifica fn_cf_cognome:");

  for (i = 0; !cf_cognome[i] || strcmp(cf_cognome[i],"FINE") ; i+=2)
  {
    fn_cf_cognome(cf_cognome[i],buf);
    buf[4] = '\0';

    if (strcmp(buf,cf_cognome[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE CF_COGNOME('%s')\n  Corretto %s\n  Calcolato %s\n",cf_cognome[i],cf_cognome[i+1],buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CF_CONTROLCHAR
   */
  ok = 1;
  printf("Verifica fn_cf_controlchar:");

  for (i = 0; !cf_cc[i] || strcmp(cf_cc[i],"FINE") ; i+=2)
  {
    fn_cf_controlchar(cf_cc[i],buf);
    buf[1] = '\0';

    if (strcmp(buf,cf_cc[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE CF_CONTROLCHAR('%s')\n  Corretto %c\n  Calcolato %c\n",cf_cc[i],cf_cc[i+1][0],buf[0]);
    }
  }

  if (ok)
    printf("\tok\n");

  /*
   *  CF_NASCITA
   */
  ok = 1;
  printf("Verifica fn_cf_nascita:");

  for (i = 0; !cf_nascita[i] || strcmp(cf_nascita[i],"FINE") ; i+=4)
  {
	unsigned anno, mese, giorno;

	fn_cf_nascita(cf_nascita[i],&anno,&mese,&giorno);

	if (  anno != (unsigned)atoi(cf_nascita[i+1]) ||
		  mese != (unsigned)atoi(cf_nascita[i+2]) ||
        giorno != (unsigned)atoi(cf_nascita[i+3]) )
    {
      ok = 0;
      printf("\n  ERRORE CF_NASCITA('%s')\n  Corretto  %s, %s, %s\n  Calcolato %d, %d, %d\n",cf_nascita[i],cf_nascita[i+1],cf_nascita[i+2],cf_nascita[i+3],anno,mese,giorno);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CF_NOME
   */
  ok = 1;
  printf("Verifica fn_cf_nome:");

  for (i = 0; !cf_nome[i] || strcmp(cf_nome[i],"FINE") ; i+=2)
  {
    fn_cf_nome(cf_nome[i],buf);
    buf[4] = '\0';

    if (strcmp(buf,cf_nome[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE CF_NOME('%s')\n  Corretto %s\n  Calcolato %s\n",cf_nome[i],cf_nome[i+1],buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CF_SESSO
   */
  ok = 1;
  printf("Verifica fn_cf_sesso:");

  for (i = 0; !cf_sesso[i] || strcmp(cf_sesso[i],"FINE") ; i+=2)
  {
    fn_cf_sesso(cf_sesso[i],buf);
    buf[1] = '\0';

    if (strcmp(buf,cf_sesso[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE CF_SESSO('%s')\n  Corretto %c\n  Calcolato %c\n",cf_sesso[i],cf_sesso[i+1][0],buf[0]);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  CIN
   */
  ok = 1;
  printf("Verifica fn_cin:");

  for (i = 0; !cin[i] || strcmp(cin[i],"FINE") ; i+=2)
  {
    fn_cin(cin[i],buf);
    buf[1] = '\0';

    if (strcmp(buf,cin[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE CIN('%s')\n  Corretto %c\n  Calcolato %c\n",cin[i],cin[i+1][0],buf[0]);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  EMAILCHECK
   */
  ok = 1;
  printf("Verifica fn_emailcheck:");

  for (i = 0; !email[i] || strcmp(email[i],"FINE") ; i+=2)
  {
    const int risposta = fn_emailcheck(email[i]);
    const int giusto = email[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE EMailCheck('%s')\n  Corretto %d\n  Calcolato %d\n",email[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  FLOATTOSTR
   */
  ok = 1;
  printf("Verifica fn_floattostr:");

  for (i = 0; !floatstr[i] || strcmp(floatstr[i],"FINE") ; i+=3)
  {
    const double d1 = floatstr[i] ? atof(floatstr[i]) : 0.0;
    const double *pd = floatstr[i] ? &d1 : 0;

    fn_floattostr(pd, floatstr[i + 1], buf);

    if (strcmp(buf, floatstr[i + 2]))
    {
      ok = 0;
      printf("\n  ERRORE FLOATTOSTR('%s',%s)\n  Corretto %s\n  Calcolato %s\n",
             floatstr[i + 1], floatstr[i], floatstr[i + 2], buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  GCD
   */
  ok = 1;
  printf("Verifica gn_gcd:");

  for (i = 0; !gcd[i] || strcmp(gcd[i],"FINE") ; i+=3)
  {
    const INTEGER i1 = gcd[  i] ? atoi(gcd[    i]) : 0;
    const INTEGER i2 = gcd[i+1] ? atoi(gcd[i + 1]) : 0;
    const INTEGER *pi1 = gcd[    i] ? &i1 : 0;
    const INTEGER *pi2 = gcd[i + 1] ? &i2 : 0;
    const INTEGER risposta = fn_gcd(pi1, pi2);
    const INTEGER giusto = atoi(gcd[i + 2]);
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n"
			 "  ERRORE GCD(%s,%s)\n"
			 "  Corretto " FMT_INTEGER "\n"
			 "  Calcolato " FMT_INTEGER "\n",
			 gcd[i], gcd[i + 1], giusto, risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  GEODIST
   */
  ok = 1;
  printf("Verifica gn_geodist:");

  d = fn_geodist(&lat_london,&lon_london,&lat_pisa,&lon_pisa);
  if ((int)d != 1167)
  {
    ok = 0;
    printf("\n  ERRORE GEODIST(londra,pisa)\n  Corretto 1167\n  Calcolato %f\n",d);
  }

  d = fn_geodist(&lat_tokyo,&lon_tokyo,&lat_pisa,&lon_pisa);
  if ((int)d != 9811)
  {
    ok = 0;
    printf("\n  ERRORE GEODIST(tokyo,pisa)\n  Corretto 9811\n  Calcolato %f\n",d);
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  GTINCHECK
   */
  ok = 1;
  printf("Verifica fn_gtincheck:");

  for (i = 0; !gtin[i] || strcmp(gtin[i],"FINE") ; i+=2)
  {
    const int risposta = fn_gtincheck(gtin[i]);
    const int giusto = gtin[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE GtinCheck('%s')\n  Corretto %d\n  Calcolato %d\n",gtin[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *   IBAN
   */
  ok = 1;
  printf("Verifica fn_iban:");

  for (i = 0; !iban[i] || strcmp(iban[i],"FINE") ; i+=3)
  {
    fn_iban(iban[i],iban[i+1],buf);

    if (strcmp(buf,iban[i+2]))
    {
      ok = 0;
      printf("\n  ERRORE IBAN('%s','%s')\n  Corretto  %s\n  Calcolato %s\n",iban[i],iban[i+1],iban[i+2],buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  IP4CHECK
   */
  ok = 1;
  printf("Verifica fn_ip4check:");

  for (i = 0; !ip4[i] || strcmp(ip4[i],"FINE") ; i+=2)
  {
    const int risposta = fn_ip4check(ip4[i]);
    const int giusto = ip4[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE IP4Check('%s')\n  Corretto %d\n  Calcolato %d\n",ip4[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  LCM
   */
  ok = 1;
  printf("Verifica fn_lcm:");

  for (i = 0; !lcm[i] || strcmp(lcm[i],"FINE") ; i+=3)
  {
    const INTEGER i1 = lcm[    i] ? atoi(lcm[  i]) : 0;
    const INTEGER i2 = lcm[i + 1] ? atoi(lcm[i+1]) : 0;
    const INTEGER *pi1 = lcm[    i] ? &i1 : 0;
    const INTEGER *pi2 = lcm[i + 1] ? &i2 : 0;
    const INTEGER risposta = fn_lcm(pi1, pi2);
    const INTEGER giusto = atoi(lcm[i + 2]);
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE LCM(%s,%s)\n"
			 "  Corretto " FMT_INTEGER "\n"
			 "  Calcolato " FMT_INTEGER "\n",
			 lcm[i], lcm[i + 1], giusto, risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  LD
   */
  ok = 1;
  printf("Verifica fn_ld:");

  for (i = 0; !ld[i] || strcmp(ld[i],"FINE") ; i+=3)
  {
    const int risposta = fn_ld(ld[i],ld[i+1]);
    const int giusto = atoi(ld[i+2]);

    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE LD('%s','%s')\n  Corretto  %d\n  Calcolato %d\n",ld[i],ld[i+1],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\t\tok\n");

  /*
   *  LUHNCHECK
   */
  ok = 1;
  printf("Verifica fn_luhncheck:");

  for (i = 0; !luhn[i] || strcmp(luhn[i],"FINE") ; i+=2)
  {
    const int risposta = fn_luhncheck(luhn[i]);
    const int giusto = luhn[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE LuhnCheck('%s')\n  Corretto %d\n  Calcolato %d\n",luhn[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  NAMEPERMUTATION
   */
  ok = 1;
  printf("Verifica fn_nameperm:");

  for (i = 0; !np[i] || strcmp(np[i], "FINE"); i += 3)
  {
    const int risposta = fn_namepermutation(np[i], np[i+1], "");
    const int giusto = np[i+2][0] == (char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE NamePermutation('%s', '%s')\n  Corretto %d\n  Calcolato %d\n", np[i], np[i+1], giusto, risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  PIVACHECK
   */
  ok = 1;
  printf("Verifica fn_piva_check:");

  for (i = 0; !piva[i] || strcmp(piva[i],"FINE") ; i+=2)
  {
    const int risposta = fn_piva_check(piva[i]);
    const int giusto = piva[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE PIvaCheck('%s')\n  Corretto %d\n  Calcolato %d\n",piva[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  SHA256
   */
  ok = 1;
  printf("Verifica fn_sha256:");

  for (i = 0; !sha256[i] || strcmp(sha256[i],"FINE") ; i+=2)
  {
    fn_sha256(sha256[i],buf);

    if (strcmp(buf,sha256[i+1]))
    {
      ok = 0;
      printf("\n  ERRORE SHA256('%s')\n  Corretto  %s\n  Calcolato %s\n",sha256[i],sha256[i+1],buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  PARTITAIVA
   */
  ok = 1;
  printf("Verifica fn_siacheck:");

  for (i = 0; !sia[i] || strcmp(sia[i],"FINE") ; i+=2)
  {
    const int risposta = fn_siacheck(sia[i]);
    const int giusto = sia[i+1][0]==(char)'0' ? 0 : 1;
    if (risposta != giusto)
    {
      ok = 0;
      printf("\n  ERRORE SiaCheck('%s')\n  Corretto %d\n  Calcolato %d\n",sia[i],giusto,risposta);
    }
  }

  if (ok)
    printf("\t\tok\n");

  /*
   *  SOUNDEX
   */
  ok = 1;
  printf ("Verifica fn_soundex:");

  for (i = 0; !soundex[i] || strcmp(soundex[i],"FINE") ; i+=2)
  {
    fn_soundex(soundex[i],buf);

    if (strcmp(buf, soundex[i + 1]))
    {
      ok = 0;
      printf("\n  ERRORE SOUNDEX('%s')\n  Corretto  %s\n  Calcolato %s\n",
             soundex[i], soundex[i + 1], buf);
    }
  }

  if (ok)
    printf("\t\tok\n");

  printf("----------------------------------------\n");
  printf("FINE TEST\n");

  return 0;
}
