/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(EASYUDF_MISC)
#define      EASYUDF_MISC

#include "easyudf.h"

void UDFEXPORT fn_bban(const char [], const char [], const char [], char *);
void UDFEXPORT fn_cin(const char [], char *);
SMALLINT UDFEXPORT fn_gtincheck(const char []);
void UDFEXPORT fn_iban(const char [2], const char [], char *);
SMALLINT UDFEXPORT fn_ip4check(const char []);
SMALLINT UDFEXPORT fn_luhncheck(const char []);
SMALLINT UDFEXPORT fn_partitaiva(const char []);
SMALLINT UDFEXPORT fn_siacheck(const char []);

#endif  /* include guard */
