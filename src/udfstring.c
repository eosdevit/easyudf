/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "udfstring.h"

/*
 *  fn_floattostr
 *
 *  Writes formatted output to string 'output'.
 *  Applies to 'd' a format specifier contained in the format string pointed to
 *  by 'fmt' and outputs the formatted data to a 'output'.
 */
void UDFEXPORT fn_floattostr(const NUMBER *d, const char fmt[], char *output)
{
  output[0] = '\0';

  if (d)
    sprintf(output, fmt, *d);
}

/*
 *  fn_ld
 *
 *  Levenshtein distance (LD) is a measure of the similarity between two
 *  strings, which we will refer to as the source string (s) and the target
 *  string (t). The distance is the number of deletions, insertions, or
 *  substitutions required to transform s into t. For example, if s is "test"
 *  and t is "test", then `LD(s,t) = 0`, because no transformations are needed.
 *  The strings are already identical. If s is "test" and t is "tent", then
 *  `LD(s,t) = 1`, because one substitution (change "s" to "n") is sufficient
 *  to transform s into t. The greater the Levenshtein distance, the more
 *  different the strings are.
 *  Levenshtein distance is named after the Russian scientist Vladimir
 *  Levenshtein, who devised the algorithm in 1965.
 *  The Levenshtein distance algorithm has been used in:
 *  - spell checking;
 *  - speech recognition;
 *  - DNA analysis;
 *  - plagiarism detection.
 *
 *  The time-complexity of this implementation is `O(|s1|*|s2|)`, the space
 *  complexity is `O(|s1|)`.
 *  It covers transposition, in addition to deletion, insertion and
 *  substitution ("An Extension of Ukkonen's Enhanced Dynamic Programming ASM
 *  Algorithm", Berghel, Hal, Roach, David).
 *  http://www.acm.org/~hlb/publications/asm/asm.html
 */
INTEGER UDFEXPORT fn_ld(const char source[], const char target[])
{
  int *rows[3] = {0,0,0}, *pp, *p, *d;

  int i, j, dist, n, m;

  if (!target || !source)
    return -1;

  m = (int)strlen(target);
  n = (int)strlen(source);

  if (!m)
    return n;

  if (!n)
    return m;

  rows[0] = (int *)malloc(sizeof(int) * ((unsigned)n + 1));
  if (!rows[0])
    return -1;

  rows[1] = (int *)malloc(sizeof(int) * ((unsigned)n + 1));
  if (!rows[1])
  {
    free(rows[0]);
    return -1;
  }

  rows[2] = (int *)malloc(sizeof(int) * ((unsigned)n + 1));
  if (!rows[2])
  {
    free(rows[0]);
    free(rows[1]);
    return -1;
  }

  pp = rows[0]; /* 'Previous-previous' cost array, horizontally */
  p = rows[1];  /* 'Previous' cost array, horizontally */
  d = rows[2];  /* Cost array, horizzontally */

  for (i = 0; i <= n; ++i)
    p[i] = i;

  for (j = 1; j <= m; ++j)
  {
    int *tmp;

    d[0] = j;

    for (i = 1; i <= n; ++i)
    {
      const int cost = source[i-1] != target[j-1];

      /* Minimum of cell to the left+1, to the top+1, diagonally left and up
         +cost. */
      const int min1 = p[i-1] + cost < p[i] + 1 ? p[i-1] + cost : p[i] + 1;
      const int min2 = d[i-1] + 1 < min1 ? d[i-1] + 1 : min1;

      d[i] = min2;

      /* Cover transposition, in addition to deletion, insertion and
         substitution. This step is taken from:
           "An Extension of Ukkonen's Enhanced Dynamic Programming ASM
            Algorithm"  (Berghel, Hal, Roach, David)

            http://www.acm.org/~hlb/publications/asm/asm.html */
      if (j > 2)
      {
        int trans = pp[i-2] + 1;

        if (source[i-2] != target[j-1])
          ++trans;

        if (source[i-1] != target[j-2])
          ++trans;

        if (d[i] > trans)
          d[i] = trans;
      }
    }

    /* 'Shift' rows. */
    tmp = pp;
    pp  = p;
    p   = d;
    d   = tmp;
  }

  /* Our last action in the above loop was to switch `d` and `p`, so `p` now
     actually has the most recent cost counts. */
  dist = p[n];

  free(rows[0]);
  free(rows[1]);
  free(rows[2]);

  return dist;
}

static int oneway_namepermutation(const char s1[], const char s2[], const char sep[])
{
  static const char default_sep[] = " ,.-'";

  const char *ptr = s1;
  char *tmpstr;
  int start, stop = 0;
  int ret = 1;

  const unsigned l1 = strlen(s1);

  if (s1 == s2 || (s1[0] == 0 && s2[0] == 0))
    return 1;

  /* Setup */
  if (!sep || sep[0] == 0)
    sep = default_sep;

  tmpstr = (char *)malloc(l1 + 1);
  if (!tmpstr)
    return 0;

  /* Search */
  do
  {
    start = stop;
    while (ptr[start] && strchr(sep, ptr[start]))
      ++start;

    stop = start;
    while (ptr[stop] && !strchr(sep, ptr[stop]))
      ++stop;

    if ((unsigned)(stop - start) > l1)  /* probably a buffer overflow attack */
    {
      ret = 0;
      break;
    }

    if (start < stop)
    {
      memcpy(tmpstr, &ptr[start], (unsigned)(stop - start));
      tmpstr[stop - start] = 0;

      if (!strstr(s2, tmpstr))
      {
        ret = 0;
        break;
      }
    }
  } while (start < stop);

  free(tmpstr);
  return ret;
}

/*
 *  fn_namepermutation
 *
 *  Compares two strings (`s1`, `s2`) disregarding the order of the words
 *  (words are separated by characters in sep).
 */
SMALLINT UDFEXPORT fn_namepermutation(const char s1[], const char s2[],
                                   const char sep[])
{
  return oneway_namepermutation(s1, s2, sep)
         && oneway_namepermutation(s2, s1, sep);
}

/*
 *  fn_soundex
 */
void UDFEXPORT fn_soundex(const char name[], char *soundEx)
{
#define inSz 255

  if (name)
  {
    char wordStr[inSz + 1], firstLetter, *p, *p2;
    unsigned i, wsLen;

    strncpy(wordStr, name, inSz);
    wordStr[inSz] = 0;

    for (i = 0; wordStr[i]; ++i)
    {
      if (isalpha(wordStr[i]))
        wordStr[i] = (char)toupper(wordStr[i]);
      else
        wordStr[i] = ' ';
    }

    wsLen = strlen(wordStr);
    if (!wsLen)
      return;

    firstLetter = wordStr[0];
    if (firstLetter == 'H' || firstLetter == 'W')
      wordStr[0] = '-';

    /* In properly done census SoundEx, the H and W will be squezed out before
       performing the test for adjacent digits (this differs from how 'real'
       vowels are handled). */
    for (p = wordStr + 1; *p; ++p)
      if (*p == 'H' || *p == 'W')
        *p = '.';

    /* Perform classic SoundEx replacements. */
    for(p = wordStr; *p; ++p)
    {
      if (strchr("AEIOUYHW",*p))
        *p = '0';
      else if (strchr("BPFV",*p))
        *p = '1';
      else if (strchr("CSGJKQXZ",*p))
        *p = '2';
      else if (*p == 'D' || *p == 'T')
        *p = '3';
      else if (*p == 'L')
        *p = '4';
      else if (*p == 'M' || *p == 'N')
        *p = '5';
      else if (*p == 'R')
        *p = '6';
    }

    /* In properly done census SoundEx, the H and W will be squezed out before
       performing the test for adjacent digits (this differs from how 'real'
       vowels are handled. */
    p2 = wordStr + 1;
    for (p = wordStr + 1; *p; ++p)
    {
      *p2 = *p;
      if (*p2 != '.')
        ++p2;
    }
    *p2 = 0;


    /* Squeeze out extra equal adjacent digits. */
    p2 = wordStr;
    for (p = wordStr; *p; ++p)
    {
      *p2 = *p;
      if (*p2 != p[1])
        ++p2;
    }
    *p2 = 0;

    /* Squeeze out spaces and zeros. Leave the first letter code to be replaced
       below. */
    p2 = wordStr+1;
    for (p = wordStr+1; *p; ++p)
    {
      *p2 = *p;
      if (*p2 != (char)' ' && *p2 != (char)'0' )
        ++p2;
    }
    *p2 = 0;

    wsLen = strlen(wordStr);

    /* Right pad with zero characters. */
    for(i = wsLen; i < 4; ++i)
      wordStr[i] = '0';

    /* Size to taste. */
    wordStr[4] = 0;

    /* Replace first digit with first letter. */
    wordStr[0] = firstLetter;

    /* Copy wordStr to soundEx. */
    strcpy(soundEx,wordStr);
  }
  else
    soundEx[0] = '\0';

#undef inSz
}
