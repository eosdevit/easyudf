/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */
#if !defined(EASYUDF_SHA)
#define      EASYUDF_SHA

#include "easyudf.h"

void UDFEXPORT fn_sha256(const char [], char *);

#endif  /* include guard */
