/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "udfsha.h"

/*
 *  fn_sha256
 *
 *  The SHA-256 (Secure Hash Algorithm) hash function is a cryptographic hash
 *  functions designed by the NSA (National Security Agency) and published as a
 *  US government standard. SHA-1 is employed in a large variety of popular
 *  security applications and protocols, including TLS and SSL, PGP, SSH,
 *  S/MIME, and IPsec. It was considered to be the successor to MD5, an
 *  earlier, widely-used hash function. Both are reportedly compromised.
 *  The other four variants (SHA-224, SHA-256, SHA-384, and SHA-512) are
 *  sometimes collectively referred to as SHA-2 functions or simply SHA-2. No
 *  attacks have yet been reported on the SHA-2 variants, but since they are
 *  similar to SHA-1, researchers are worried, and are developing candidates
 *  for a new, better hashing standard.
 *  SHA-256 is a hash function computed with 32-bit words. It have not received
 *  as much scrutiny by the public cryptographic community as SHA-1 has, and so
 *  their cryptographic security is not yet as well-established. Gilbert and
 *  Handschuh (2003) have studied the newer variant and found no weaknesses.
 *  SHA-256 is a required secure hash algorithms for use in U.S. Federal
 *  applications, including use by other cryptographic algorithms and
 *  protocols, for the protection of sensitive unclassified information. FIPS
 *  PUB 180-1 also encouraged adoption and use of SHA-1 by private and
 *  commercial organizations.
 *  A prime motivation for the publication of the Secure Hash Algorithm was the
 *  Digital Signature Standard, in which it is incorporated.
 *  The SHA hash functions have been used as the basis for the SHACAL block
 *  ciphers.
 */
#define block_size 512

struct sha_state
{
  uint32_t            state[8];
  uint32_t buf[block_size / 4];

  unsigned length;
  unsigned curlen;
};

static const uint32_t K[64] =
{
  0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
  0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
  0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
  0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
  0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
  0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
  0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
  0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
  0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
  0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
  0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
  0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
  0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
  0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
  0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
  0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL
};

/* Various logical functions. */
#define shaCh(x, y, z)  ( ((x) & (y)) ^ (~(x) & (z)) )
#define shaMaj(x, y, z)  ( ((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)) )
#define shaS(x, n)  ( ((x)>>((n)&31)) | ((x)<<(32-((n)&31))) )
#define shaR(x, n)  ( (x) >> (n) )
#define shaSigma0(x)  ( shaS(x,2) ^ shaS(x,13) ^ shaS(x,22) )
#define shaSigma1(x)  ( shaS(x,6) ^ shaS(x,11) ^ shaS(x,25) )
#define shaGamma0(x)  ( shaS(x,7) ^ shaS(x,18) ^ shaR(x, 3) )
#define shaGamma1(x)  ( shaS(x,17) ^ shaS(x,19) ^ shaR(x,10) )

/* Compress 512-bits. */
static void sha_compress(struct sha_state *md)
{
  unsigned i;

  uint32_t S[ 8];
  uint32_t W[64];

  uint32_t t0, t1;

  /* Copy state into S. */
  for (i = 0; i < 8; ++i)
    S[i] = md->state[i];

  /* Copy the state into 512-bits into W[0..15]. */
  for (i = 0; i < 16; ++i)
    W[i] = ( (md->buf[(4 * i)    ] << 24) |
             (md->buf[(4 * i) + 1] << 16) |
             (md->buf[(4 * i) + 2] <<  8) |
	     (md->buf[(4 * i) + 3]      ) );

  /* Fill `W[16..63]`. */
  for (i = 16; i < 64; ++i)
    W[i] = shaGamma1(W[i-2]) + W[i-7] + shaGamma0(W[i-15]) + W[i-16];

  /* Compress. */
  for (i = 0; i < 64; ++i)
  {
    t0 = S[7] + shaSigma1(S[4]) + shaCh(S[4],S[5],S[6]) + K[i] + W[i];
    t1 = shaSigma0(S[0]) + shaMaj(S[0],S[1],S[2]);

    S[7] = S[6];
    S[6] = S[5];
    S[5] = S[4];
    S[4] = S[3] + t0;
    S[3] = S[2];
    S[2] = S[1];
    S[1] = S[0];
    S[0] = t0 + t1;
  }

  /* Feedback. */
  for (i = 0; i < 8; ++i)
    md->state[i] += S[i];
}

/* Init the SHA state. */
static void sha_init(struct sha_state *md)
{
  md->curlen = md->length = 0;
  md->state[0] = 0x6A09E667UL;
  md->state[1] = 0xBB67AE85UL;
  md->state[2] = 0x3C6EF372UL;
  md->state[3] = 0xA54FF53AUL;
  md->state[4] = 0x510E527FUL;
  md->state[5] = 0x9B05688CUL;
  md->state[6] = 0x1F83D9ABUL;
  md->state[7] = 0x5BE0CD19UL;
}

static void sha_process(struct sha_state *md, const char *buf, int len)
{
  while (len--)
  {
    md->buf[md->curlen++] = (unsigned)*buf++;  /* copy byte */

    if (md->curlen == 64)  /* is 64 bytes full? */
    {
      sha_compress(md);
      md->length += 512;
      md->curlen = 0;
    }
  }
}

static void sha_done(struct sha_state *md, unsigned char *hash)
{
  unsigned i;

  md->length += md->curlen * 8;  /* increase the length of the message */

  md->buf[md->curlen++] = 0x80;  /* append the '1' bit */

  /* If the length is currenlly above 56 bytes we append zeros
     then compress.  Then we can fall back to padding zeros and length
     encoding like normal. */
  if (md->curlen >= 56)
  {
    for (; md->curlen < 64;)
      md->buf[md->curlen++] = 0;
    sha_compress(md);
    md->curlen = 0;
  }

  /* Pad upto 56 bytes of zeroes. */
  for (; md->curlen < 56;)
    md->buf[md->curlen++] = 0;

  /* Since all messages are under 2^32 bits we mark the top bits zero. */
  for (i = 56; i < 60; ++i)
    md->buf[i] = 0;

  /* Append length. */
  for (i = 60; i < 64; ++i)
    md->buf[i] = (md->length >> ((63-i) * 8)) & 255;
  sha_compress(md);

  /* Copy output. */
  for (i = 0; i < 32; ++i)
    hash[i] = (md->state[i >> 2] >> (((3-i) & 3) << 3)) & 255;
}

void UDFEXPORT fn_sha256(const char buf[], char *hash)
{
  if (buf)
  {
    static const char hex[] = {'0','1','2','3','4','5','6','7','8','9',
                               'a','b','c','d','e','f'};

    const unsigned len = strlen(buf);
    const unsigned blocks = len / block_size;

    struct sha_state md;

    unsigned char h[32];

    unsigned i;

    sha_init(&md);

    for (i = 0; i < blocks; ++i)
      sha_process(&md,buf+(i*block_size),512);
    sha_process(&md,buf+blocks*block_size,len%block_size);

    sha_done(&md,h);

    for (i = 0; i < 64; i+=2)
    {
      hash[i  ] = hex[ (h[i/2] & 0xF0) >> 4];
      hash[i+1] = hex[ (h[i/2] & 0x0F)     ];
    }
    hash[64] = 0;
  }
  else  /* `buf` is null */
    hash[0] = '\0';
}
