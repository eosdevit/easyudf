/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(EASYUDF_MATH)
#define      EASYUDF_MATH

#include "easyudf.h"

INTEGER UDFEXPORT fn_gcd(const INTEGER *, const INTEGER *);
NUMBER UDFEXPORT fn_geodist(const NUMBER *const, const NUMBER *const,
                            const NUMBER *const, const NUMBER *const);
INTEGER UDFEXPORT fn_lcm(const INTEGER *const, const INTEGER *const);

#endif  /* include guard */
