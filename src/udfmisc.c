/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <assert.h>
#include <ctype.h>
#include <string.h>

#include "udfmisc.h"
#include "util.h"

/**
 *  Il BBAN (Basic Bank Account Number) � la coordinata bancaria nazionale che
 *  consente di identificare, in maniera standard ed univoca, il conto corrente
 *  del beneficiario permettendo all'ordinante, o alla banca di quest'ultimo, di
 *  verificarne la correttezza grazie alla presenza del carattere di controllo.
 *  La struttura del BBAN � fissa ed � stata dettata, per il nostro paese, da
 *  standard dell'ABI (Associazione Bancaria Italiana); in particolare la
 *  lunghezza di 23 caratteri � fissa ed � cos� composta:
 *
 *        CIN             ABI             CAB            NUMERO CONTO
 *       codice                                               12
 *         di                                              caratteri
 *      controllo
 *    (UNA LETTERA)  (CINQUE CIFRE)  (CINQUE CIFRE)  (MAIUSCOLE E NUMERI)
 *
 *         X              06055           00000          000000000000
 *
 *  Il BBAN non pu� contenere caratteri speciali, deve essere composto da
 *  numeri e lettere maiuscole; il numero conto effettivo deve essere
 *  allineato a destra e riempito, eventualmente, di zeri a sinistra fino
 *  all'effettivo riempimento del campo.
 *  Il primo carattere del BBAN (CIN) ha la funzione di carattere di controllo
 *  dell'esatta trascrizione dei successivi 22 caratteri.
 *  Il CIN deve essere un carattere alfabetico, l'ABI e il CAB numerici.
 *  Dal 16 giugno 2003 l'esecuzione di bonifici Italia con coordinate bancarie
 *  incomplete o inesatte � assoggettata ad una penale a carico dell'ordinante
 *  del bonifico (per il momento il carattere di controllo CIN rimane un dato
 *  facoltativo non assoggettato a penale).
 *  Il codice BBAN � riportato su ogni estratto conto e su ogni contabile di
 *  conto corrente.
 */
void UDFEXPORT fn_bban(const char abi[], const char cab[], const char cc[],
                    char *bban)
{
  assert(bban);
  bban[0] = 0;

  if (abi && abi[0] && isdigit(abi[0]) &&
      cab && cab[0] && isdigit(cab[0]) &&
      cc && cc[0])
  {
    const size_t max_abi = 5;
    const size_t max_cab = 5;
    const size_t max_cc = 12;

    const size_t n_abi = strlen(abi);
    const size_t n_cab = strlen(cab);
    const size_t n_cc = strlen(cc);

    char *pos = bban + 1;
    size_t i;

    if (n_abi > max_abi)
      return;

    if (n_cab > max_cab)
      return;

    if (n_cc > max_cc)
      return;

    for (i = 0; i < max_abi - n_abi; ++i)
      *pos++ = '0';
    for (i = 0; i < max_abi && abi[i]; ++i)
      *pos++ = abi[i];

    for (i = 0; i < max_cab - n_cab; ++i)
      *pos++ = '0';
    for (i = 0; i < max_cab && cab[i]; ++i)
      *pos++ = cab[i];

    for (i = 0; i < max_cc - n_cc; ++i)
      *pos++ = '0';
    for (i = 0; i < max_cc && cc[i]; ++i)
      *pos++ = cc[i];

    *pos = 0;

    fn_cin(bban + 1, bban);
  }
}

/**
 *  A partire dall'ormai lontana circolare ABI del 01/06/1992 � stato stabilito
 *  che per facilitare il trattamento automatico delle disposizioni bancarie e
 *  per verificare la correttezza dei dei conti correnti oggetto della
 *  transazione era necessario introdurre una check-digit.
 *
 *  Il codice � di 22 cifre + 1 composte secondo la codifica
 *  DAAAAABBBBBCCCCCCCCCCCC dove:
 *    A - � il codice ABI su cinque caratteri allineato a destra
 *    B - � il codice CAB su cinque caratteri allineato a destra
 *    C - � il conto corrente allineato a sinistra con eventuali blank
 *        riempitivi a destra
 *    D - � la check-digit, il CIN (Acronimo di Control Internal Number)
 *
 *  Tale sequenza di 23 cifre costituisce il codice BBAN (Basik Bank Account
 *  Number) utilizzato per convenzione in Italia per le transazioni
 *  automatizzate.
 *
 *  Il BBAN � utilizzato per le transazioni "domestiche", per il regolamento di
 *  transazioni internazionali � richiesto il codice IBAN (International Bank
 *  Account Number) che al BBAN aggiunge il codice nazione (IT nel nostro caso)
 *  ed i caratteri di controllo risultanti dalla codifica.
 *
 *  Dal giugno 2003 l'errata indicazione del codice BBAN in una transazione
 *  comporta una penale a carico dell'ordinante il bonifico e/o la respinta
 *  dell'intera coda di comandi (nel caso di RID o RIBA), indicare il codice
 *  corretto � pertanto diventato molto importante.
 *
 *  Il calcolo / verifica del CIN � molto simile al calcolo della check-digit
 *  del codice fiscale: esiste una tabella "pesi assegnati" ai caratteri pari
 *  ed una tabella dei pesi per i caratteri dispari.
 *
 *  La stringa di 22 caratteri va letta da sinistra a destra; va totalizzato il
 *  peso di ciascun carattere incontrato, il valore di ciascun carattere va
 *  desunto dalle tabelle peso caratteri pari, peso caratteri dispari.
 *
 *  Il totale / somma ottenuta va divisa per 26. Il resto della divisione
 *  intera � utilizzato come indice per ricercare in una apposita tabella.
 *
 *  La funzione si aspetta in ingresso una stringa di 22 caratteri (codice BBAN
 *  correttamente formattato, con caratteri maiuscoli) e calcola il carattere
 *  CIN.
 */
void UDFEXPORT fn_cin(const char codice[], char *cin)
{
  static const char numeri[]  = "0123456789";
  static const char lettere[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ-. ";
  static const unsigned listaPari[] =
    { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
     17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28};
  static const unsigned listaDispari[] =
    {1, 0,  5,  7,  9, 13, 15, 17, 19, 21,  2,  4, 18, 20, 11, 3,
     6, 8, 12, 14, 16, 10, 22, 25, 24, 23, 27, 28, 26};

  int somma = 0;
  size_t k;

  if (!cin)
    return;
  *cin = '?';

  if (!codice || strlen(codice) != 22)  /* 22 = 5 (ABI) + 5 (CAB) + 12 (CC) */
    return;

  /* Calcolo valori caratteri. */
  for (k = 0; k < 22; ++k)
  {
    size_t i;
    const char *c = strchr(numeri, codice[k]);

    if (c)
      i = (size_t)(c - numeri);
    else
    {
      c = strchr(lettere, codice[k]);
      if (c)
        i = (size_t)(c - lettere);
      else
        return;
    }

    somma += (k % 2) ? listaPari[i] : listaDispari[i];
  }

  *cin = lettere[somma % 26];
}

/**
 *  The International Bank Account Number (IBAN) is an international standard
 *  for identifying bank accounts across national borders. It was originally
 *  adopted by the European Committee for Banking Standards, and was later
 *  adopted as ISO 13616:1997 and now as ISO 13616:2003. The official IBAN
 *  registrar under ISO 13616:2003 is SWIFT and the IBAN registry is currently
 *  at SWIFT.
 *
 *  The IBAN was developed to facilitate payments within the European Union.
 *  Customers, especially individuals and SMEs, are frequently confused by
 *  differing national standards for bank account numbers. The IBAN is not yet
 *  used for routing, because the IBAN has not been widely adopted outside
 *  Europe, among other reasons. The ECBS expects that adoption may take up to
 *  ten years, so it remains necessary to use the current ISO 9362 Bank
 *  Identifier Code system (BIC or SWIFT code) in conjunction with the BBAN or
 *  IBAN.
 *
 *  At present, the United States does not participate in IBAN. Any adoption of
 *  the IBAN standard by U.S. banks would likely be initiated by ANSI ASC X9,
 *  the U.S. financial services standards development organization.
 *  Currently all European non-CIS countries, as well as some African
 *  countries, and Turkey participate in the IBAN system, while the rest of
 *  the world remains outside of it. British dependencies (except Gibraltar
 *  and the  Crown Dependencies) don't participate in the IBAN system.
 *  The IBAN consists of a ISO 3166-1 alpha-2 country code, followed by two
 *  check digits (represented by kk in the examples below), and up to thirty
 *  alphanumeric characters for the domestic bank account number, called the
 *  BBAN (Basic Bank Account Number). It is up to each country's national
 *  banking community to decide on the length of the BBAN for accounts in that
 *  country, but its length must be fixed for any given country.
 *
 *  The IBAN must not contain spaces when stored electronically. When printed
 *  on paper, however, the norm is to express it in groups of four characters,
 *  the last group being of variable length.
 */
void UDFEXPORT fn_iban(const char nazione[2], const char bban[], char *iban)
{
  static const char alpha[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  assert(iban);
  iban[0] = 0;

  if (nazione && strlen(nazione) == 2 &&
      isalpha(nazione[0]) && isalpha(nazione[1]) && bban)
  {
    char s[128] = "", cds[3];

    unsigned sum = 0, mul = 1, cd, i;

    strcpy(iban, bban);
    strcat(iban, nazione);
    strcat(iban, "00");

    for (i = 0; iban[i]; ++i)
    {
      const char *c = strchr(alpha, iban[i]);

      if (c)
      {
        char d[] = "00";

        d[0] = eudf_uint_to_dgt((10 + (size_t)(c - alpha)) / 10);
        d[1] = eudf_uint_to_dgt((10 + (size_t)(c - alpha)) % 10);

        strcat(s, d);
      }
      else
      {
        char d[] = "0";

        d[0] = iban[i];

        strcat(s, d);
      }
    }

    for (i = strlen(s); i-- > 0;)
    {
      sum += (eudf_dgt_to_uint(s[i]) * mul);
      sum %= 97;
      mul *= 10;
      mul %= 97;
    }

    cd = 98 - sum;

    cds[0] = eudf_uint_to_dgt(cd / 10);
    cds[1] = eudf_uint_to_dgt(cd % 10);
    cds[2] = 0;

    strcpy(iban, nazione);
    strcat(iban, cds);
    strcat(iban, bban);
  }
}

/**
 *  Returns 0 if 'code' is not a valid GTIN string.
 */
SMALLINT UDFEXPORT fn_gtincheck(const char code[])
{
  if (code)
  {
    const size_t l = strlen(code);
    size_t i;
    unsigned sum = 0, digit, check;

    if (l != 8 && l != 12 && l != 13 && l != 14 && l != 18)
      return 0;

    for (i = 0; i < (l - 1); ++i)
    {
      if (!isdigit(code[i]))
        return 0;

      digit = eudf_dgt_to_uint(code[i]);
      sum += ((l + i) % 2 ? 1 : 3) * digit;
    }

    if (!isdigit(code[l - 1]))
      return 0;

    check = eudf_dgt_to_uint(code[l - 1]);

    return (10 - sum % 10) == check;
  }

  return 0;
}

/**
 *  Returns 0 is 'ip' is not a valid ip number.
 */
SMALLINT UDFEXPORT fn_ip4check(const char ip[])
{
  if (ip)
  {
    char octet[3];
    unsigned digits = 0, dots = 0, val;

    const char *p;

    if (strlen(ip) > 15)
      return 0;

    for (p = ip; *p; ++p)
    {
      if (isdigit(*p))
      {
        if (digits > 2)
          return 0;

        octet[digits++] = *p;
      }
      else if (*p == '.')
      {
        ++dots;
        if (dots > 3)
          return 0;

        if (!digits)
          return 0;

        val = (eudf_dgt_to_uint(octet[digits - 1])
              + (digits>1 ? eudf_dgt_to_uint(octet[digits - 2]) *  10  : 0)
              + (digits>2 ? eudf_dgt_to_uint(octet[digits - 3]) * 100 :  0));

        if (val > 255)
          return 0;

        digits = 0;
      }
      else
        return 0;
    }

    return dots == 3;
  }

  return 0;
}

/**
 *  Returns 0 if 'code' is not a valid Luhn number.
 */
SMALLINT UDFEXPORT fn_luhncheck(const char code[])
{
  if (code)
  {
    const unsigned n_digits = strlen(code);

    unsigned sum = 0, i;

    for (i = 0; i < n_digits; ++i)
    {
      const unsigned digit = eudf_dgt_to_uint(code[i]);

      sum += (i & 1) ? (digit << 1) % 9 : digit;
    }

    return sum % 10 == 0;
  }

  return 0;
}

/**
 *  Restituisce 0 se 'piva' non � una partita iva valida.
 */
SMALLINT UDFEXPORT fn_partitaiva(const char piva[])
{
  unsigned cod = 0, i, cifra;

  if (strlen(piva) != 11)
    return 0;

  for (i = 0; i < 10; ++i)
  {
    cifra = eudf_dgt_to_uint(piva[i]);

    if (!isdigit(piva[i]))
      return 0;

    cod += (i & 1) ? (2 * cifra) / 10 + (2 * cifra) % 10 : cifra;
  }

  cod = (10 - cod % 10) % 10;
  return cod == eudf_dgt_to_uint(piva[10]);
}

/**
 *  Verifica che il  codice passato abbia il formato di un codice Sia
 *  (utilizzato dalla banca per identificare un'azienda).
 */
SMALLINT UDFEXPORT fn_siacheck(const char sia[])
{
  return sia &&
         strlen(sia) == 5 &&
         isdigit(sia[0]) &&
         isdigit(sia[1]) &&
         isdigit(sia[2]) &&
         isdigit(sia[3]) &&
         isalpha(sia[4]);
}
