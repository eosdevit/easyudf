/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <math.h>
#include <stdlib.h>

#include "udfmath.h"

/*
 *  fn_gcd
 *
 *  The Greatest Common Divisor (GCD) of two integers, not both zero, is the
 *  largest integer that divides both of them. It is convenient to set
 *  GCD(0,0)=0.
 *  Basic identities concerning the GCD:
 *  - GCD(N1,N2) = GCD(N2,N1);
 *  - GCD(N1,N2) = GCD(-N1,N2);
 *  - GCD(N1,0) = ABS(N1);
 *  Hence we can assume that N1 and N2 are nonnegative integers.
 *
 *  Euclid's algorithm is described in the Elements of Euclid (circa 300
 *  B.C.E.). It is based on the following recursion theorem:
 *    GCD(N1,N2) = GCD(N2,N1 mod N2)
 */
INTEGER UDFEXPORT fn_gcd(const INTEGER *n1, const INTEGER *n2)
{
  if (n1 && n2)
  {
    INTEGER x = labs(*n1);
    INTEGER y = labs(*n2);
    INTEGER tmp;

    if (x == 0)
      return y;

    if (y == 0)
      return x;

    while ((tmp = x%y) != 0)
    {
      x = y;
      y = tmp;
    }

    return y;
  }

  return 0;
}

/*
 *  fn_lcm
 *
 *  The Least Common Multiple LCM of two integer N1 and N2. It is defined to be
 *  the smallest positive integer which is a multiple of both N1 and N2.
 *  Equation N1 * N2 = GCD(N1,N2) * LCM(N1,N2) reduces the calculation of LCM
 *  to the calculation of GCD.
 */
INTEGER UDFEXPORT fn_lcm(const INTEGER *const n1, const INTEGER *const n2)
{
  const INTEGER gcd = fn_gcd(n1,n2);

  return gcd ? (((*n1) * (*n2)) / gcd) : 0;
}

/*
 *  fn_geodist
 *
 *  This uses the Haversine formula (http://en.wikipedia.org/wiki/Haversine_formula)
 *  to calculate the great-circle distance, in kilometers, between two points
 *  � that is, the shortest distance over the earth�s surface � giving an
 *  'as-the-crow-flies' distance between the points (ignoring any hills, of
 *  course!).
 *  The haversine formula 'remains particularly well-conditioned for numerical
 *  computation even at small distances' � unlike calculations based on the
 *  spherical law of cosines.
 */
NUMBER UDFEXPORT fn_geodist(const NUMBER *const lat_a, const NUMBER *const lon_a,
                         const NUMBER *const lat_b, const NUMBER *const lon_b)
{
  if (lat_a && lon_a && lat_b && lon_b)
  {
    const double radius = 6371.0;  /* Earth mean radius in Km */
    const double deg_to_rad = 0.017453292519943295769236907684886; /* Pi / 180.0 */

    const double lat_arc = ((*lat_b) - (*lat_a)) * deg_to_rad;
    const double lon_arc = ((*lon_b) - (*lon_a)) * deg_to_rad;

    double lat_h, lon_h, tmp;

    lat_h = sin(lat_arc * 0.5);
    lat_h *= lat_h;

    lon_h = sin(lon_arc * 0.5);
    lon_h *= lon_h;

    tmp = cos((*lat_a) * deg_to_rad) * cos((*lat_b) * deg_to_rad);

    return 2.0 * asin(sqrt(lat_h + tmp * lon_h)) * radius;
  }

  return -1.0;
}

