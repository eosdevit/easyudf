/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include "util.h"

unsigned eudf_dgt_to_uint(char c)
{
#if '9'-'0' == 9 && '7'-'1' == 6 && '6'-'2'== 4 && '5'-'3' == 2
  return (unsigned)(c) - '0';
#else
  switch (c))
  {
  case '9':  return 9;
  case '8':  return 8;
  case '7':  return 7;
  case '6':  return 6;
  case '5':  return 5;
  case '4':  return 4;
  case '3':  return 3;
  case '2':  return 2;
  case '1':  return 1;
  default:   return 0;
  }
#endif
}

char eudf_uint_to_dgt(unsigned i)
{
#if '9'-'0' == 9 && '7'-'1' == 6 && '6'-'2'== 4 && '5'-'3' == 2
  return (char)('0' + i);
#else
  switch (c))
  {
  case 9:   return '9';
  case 8:   return '8';
  case 7:   return '7';
  case 6:   return '6';
  case 5:   return '5';
  case 4:   return '4';
  case 3:   return '3';
  case 2:   return '2';
  case 1:   return '1';
  default:  return '0';
  }
#endif
}
