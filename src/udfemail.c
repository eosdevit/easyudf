/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <ctype.h>
#include <string.h>

#include "udfemail.h"

/**
 *  Returns 0 if 'email' is not a valid e-mail address.
 */
SMALLINT UDFEXPORT fn_emailcheck(const char email[])
{
/* RFC2821 & RFC2822 limits. */
#define max_localpart_len  64
#define max_domain_len    255

  if (email)
  {
    const char *at = 0, *p;

    char localpart[max_localpart_len + 1], domain[max_domain_len + 1];

    size_t domain_len, localpart_len;

    if (strstr(email, ".."))
      return 0;

    for (p = email; *p; ++p)
      if (*p == '@')
      {
        if (at)
          return 0;
        at = p;
      }
      else if (!isalpha(*p) && !isdigit(*p)
               && *p != '.' && *p != '_' && *p != '-')
        return 0;

    if (!at)
      return 0;

    /* Extract e-mail localpart. */
    localpart_len = (size_t)(at - email);
    if (localpart_len > max_localpart_len)
      return 0;
    strncpy(localpart, email, localpart_len);
    localpart[localpart_len] = 0;

    /* Exctract e-mail domain/number. */
    domain_len = strlen(email)-localpart_len-1;
    if (domain_len > max_domain_len || domain_len < 4)
      return 0;
    strncpy(domain, at + 1, domain_len);

    return strchr(domain,'.')
           && domain[0] != (char)'.'
           && domain[domain_len-1] != (char)'.'
           && domain[domain_len-2] != (char)'.';
  }

  return 0;

#undef max_localpart_len
#undef max_domain_len
}
