/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(EASYUDF)
#define      EASYUDF

typedef long   INTEGER;
typedef double  NUMBER;
typedef short SMALLINT;

#define FMT_INTEGER  "%ld"
#define FMT_NUMBER    "%f"
#define FMT_SMALLINT "%hd"

#if defined __BORLANDC__ && defined __WIN32__
#  define UDFEXPORT __export
#else
#  define UDFEXPORT
#endif

#include "udfcf.h"
#include "udfemail.h"
#include "udfmath.h"
#include "udfmisc.h"
#include "udfpiva.h"
#include "udfsha.h"
#include "udfstring.h"

#endif  /* include guard */
