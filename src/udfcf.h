/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(EASYUDF_CF)
#define      EASYUDF_CF

#include "easyudf.h"

SMALLINT UDFEXPORT fn_cf_check(const char []);
void UDFEXPORT fn_cf_cognome(const char [], char *);
void UDFEXPORT fn_cf_controlchar(const char [], char *);
void UDFEXPORT fn_cf_nascita(const char [], unsigned *, unsigned *, unsigned *);
void UDFEXPORT fn_cf_nome(const char [], char *);
void UDFEXPORT fn_cf_sesso(const char [], char *);

#endif  /* include guard */
