/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <ctype.h>
#include <string.h>

#include "udfpiva.h"
#include "util.h"

/*
 *  fn_partitaiva
 *
 *  Restituisce 0 se 'piva' non e' una partita iva valida.
 */
SMALLINT UDFEXPORT fn_piva_check(const char piva[])
{
  unsigned cod = 0, i, cifra;

  if (!piva || strlen(piva) != 11)
    return 0;

  for (i = 0; i < 10; ++i)
  {
    cifra = eudf_dgt_to_uint(piva[i]);

    if (!isdigit(piva[i]))
      return 0;

    cod += (i & 1) ? (2*cifra)/10 + (2*cifra)%10 : cifra;
  }

  cod = (10 - cod%10) % 10;
  return cod == eudf_dgt_to_uint(piva[10]);
}

