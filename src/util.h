/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */


#if !defined(EASYUDF_UTIL_H)
#define      EASYUDF_UTIL_H

extern unsigned eudf_dgt_to_uint(char);
extern char eudf_uint_to_dgt(unsigned);

#endif  /* include guard */
