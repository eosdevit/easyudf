/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <ctype.h>
#include <stddef.h>

#include "udfcf.h"
#include "util.h"

static const char mesi[12] = {'A', 'B', 'C', 'D', 'E', 'H',
                              'L', 'M', 'P', 'R', 'S', 'T'};

static const unsigned giorni[12] = {31, 29, 31, 30, 31, 30,
                                    31, 31, 30, 31, 30, 31};


/*
 *  cf_consonanti_vocali
 */
static void cf_consonanti_vocali(const char str[],
                                 char *con, unsigned *ncon,
                                 char *voc, unsigned *nvoc)
{
  size_t i;

  if (!con || !ncon || !voc || !nvoc)
    return;

  *con = *voc = 0;
  *ncon = *nvoc = 0;

  if (!str)
    return;

  /* Estrazione vocali e consonanti. */
  for (i = 0; str[i] && *ncon < 4; ++i)
    if (isalpha(str[i]))
    {
      if (toupper(str[i]) == 'A' || toupper(str[i]) == 'E' ||
          toupper(str[i]) == 'I' || toupper(str[i]) == 'O' ||
          toupper(str[i]) == 'U')  /* vocale? */
      {
        if (*nvoc < 3)
          voc[(*nvoc)++] = str[i];
      }
      else  /* consonante */
        con[(*ncon)++] = str[i];
    }
}

/**
 *  Calcola e restituisce (parametri anno, mese, giorno, sesso) la data di
 *  nascita ed il sesso in base al codice fiscale.
 */
static void cf_nascita_sesso(const char cf[],
                             unsigned *anno, unsigned *mese, unsigned *giorno,
                             char *sesso)
{
  if (!anno || !mese || !giorno || !sesso)
    return;


  *anno   =  0;
  *mese   = 13;
  *giorno =  0;

  *sesso  = '?';

  if (cf)
  {
    const char m = (char)toupper(cf[8]);

    /* Estrazione dell'anno. */
    *anno = eudf_dgt_to_uint(cf[6]) * 10 + eudf_dgt_to_uint(cf[7]);

    /* Estrazione del mese. */
    *mese = 0;
    while (*mese < 12 && m != mesi[*mese])
      ++(*mese);

    if (*mese > 12)
      *mese = 0;

    /* Estrazione del giorno. */
    *giorno = eudf_dgt_to_uint(cf[9]) * 10 + eudf_dgt_to_uint(cf[10]);

    if (41 <= *giorno && *giorno <= 71)
    {
      *giorno -= 40;
      *sesso = 'F';
    }
    else
      *sesso = 'M';

    if (*giorno > giorni[*mese])
    {
      *giorno =   0;
      *sesso  = '?';
    }
  }
}

/**
 *  Verifica che cf sia un codice fiscale valido (assicurandosi che
 *  lettere/cifre siano al posto giusto ed in numero sufficiente e che il
 *  carattere di controllo sia valido).
 *  Restituisce `0` se il controllo non viene superato.
 */
SMALLINT UDFEXPORT fn_cf_check(const char cf[])
{
  if (cf)
  {
    const char mese = (char)toupper(cf[8]);
    unsigned giorno = eudf_dgt_to_uint(cf[9]) * 10 + eudf_dgt_to_uint(cf[10]), i;
    char cc;

    /* Il codice fiscale e' composto da sedici caratteri. */
    for (i = 0; i < 16; ++i)
      if (!cf[i])
        return 0;

    /* Controllo del formato generale. */
    if (!(isalpha(cf[ 0]) && isalpha(cf[ 1]) && isalpha(cf[ 2]) &&
          isalpha(cf[ 3]) && isalpha(cf[ 4]) && isalpha(cf[ 5]) &&
          isdigit(cf[ 6]) && isdigit(cf[ 7]) &&
          isalpha(cf[ 8]) &&
          isdigit(cf[ 9]) && isdigit(cf[10]) &&
          isalpha(cf[11]) &&
          isdigit(cf[12]) && isdigit(cf[13]) && isdigit(cf[14]) &&
          isalpha(cf[15])) )
      return 0;

    /* Controllo del mese. */
    i = 0;
    while (i < 12)
      if (mese == mesi[i])  break;
      else                    ++i;
    if (i > 12)
      return 0;

    /* Controllo del giorno. */
    if (41 <= giorno && giorno <= 71)
      giorno -= 40;
    if (giorno > giorni[i])
      return 0;

    /* Verifica della correttezza del carattere finale di controllo. */
    fn_cf_controlchar(cf, &cc);
    return cf[15] == cc;
  }

  return 0;
}

/**
 *  Estrae dalla stringa str 3 caratteri riassuntivi del del cognome seguendo il
 *  sistema utilizzato per il codice fiscale. Il risultato viene memorizzato nel
 *  parametro sigla.
 *  Se l'input e' nullo restituisce la stringa "XXX".
 */
void UDFEXPORT fn_cf_cognome(const char str[], char *sigla)
{
  if (!sigla)
    return;

  sigla[0] = sigla[1] = sigla[2] = 'X';
  sigla[3] = 0;

  if (str)
  {
    char con[4], voc[3];
    unsigned ncon, nvoc, inserite = 0, i;

    /* Estrazione vocali e consonanti. */
    cf_consonanti_vocali(str, con, &ncon, voc, &nvoc);

    /* Si inseriscono tutte le consonanti sino ad un massimo di tre. */
    for (i = 0; inserite < 3 && i < ncon; ++i)
      sigla[inserite++] = (char)toupper(con[i]);

    /* Se c'e' spazio residuo si inseriscono le vocali disponibili sino ad un
       massimo di tre. */
    for (i = 0; inserite < 3 && i < nvoc; ++i)
      sigla[inserite++] = (char)toupper(voc[i]);

    /* Se c'e' ancora spazio lo si riempie con delle X. */
    while (inserite < 3)
      sigla[inserite++] = 'X';
  }
}

/**
 *  Calcola e restituisce il carattere di controllo per i primi 15 caratteri
 *  del codice fiscale. Se non ci sono quindici caratteri a disposizione
 */
#if 'A'<'B' && 'B'<'C' && 'C'<'D' && 'D'<'E' && 'E'<'F' && 'F'<'G' && \
    'G'<'H' && 'H'<'I' && 'I'<'J' && 'J'<'K' && 'K'<'L' && 'L'<'M' && \
    'M'<'N' && 'N'<'O' && 'O'<'P' && 'P'<'Q' && 'Q'<'R' && 'R'<'S' && \
    'S'<'T' && 'T'<'U' && 'U'<'V' && 'V'<'W' && 'W'<'X' && 'X'<'Y' && \
    'Y'<'Z' &&                                                        \
    '0'<'1' && '1'<'2' && '2'<'3' && '3'<'4' && '4'<'5' && '5'<'6' && \
    '6'<'7' && '7'<'8' && '8'<'9'
void UDFEXPORT fn_cf_controlchar(const char cf[], char *cc)
{
  const unsigned cod_cifra[2][10] =
  {
    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
    { 1, 0, 5, 7, 9,13,15,17,19,21 }
  };
  const unsigned cod_lettera[2][26] =
  {
    {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25},
    {1,0,5,7,9,13,15,17,19,21,2,4,18,20,11,3,6,8,12,14,16,10,22,25,24,23}
  };

  *cc = '?';

  if (cf)
  {
    unsigned cod = 0, i;

    for (i = 0; i < 15 && cf[i]; ++i)
    {
      const unsigned dispari = !(i & 1);

      cod += isdigit(cf[i]) ?   cod_cifra[dispari][eudf_dgt_to_uint(cf[i])]
                            : cod_lettera[dispari][cf[i]-'A'];
    }

    if (i == 15)
      *cc = 'A' + cod % 26;
  }
}
#else
#  error "Bisogna modificare la funzione fn_cf_controlchar"
#endif

/**
 *  Calcola e restituisce (parametri anno, mese, giorno) la data di nascita
 *  codificata nel codice fiscale.
 */
void UDFEXPORT fn_cf_nascita(const char cf[],
                          unsigned *anno, unsigned *mese, unsigned *giorno)
{
  char sesso;

  cf_nascita_sesso(cf, anno, mese, giorno, &sesso);

  if (*mese != 13)
    *mese += 1;
}

/**
 *  Estrae dalla stringa str 3 caratteri riassuntivi del nome seguendo il
 *  sistema utilizzato per il codice fiscale. Il risultato viene memorizzato nel
 *  parametro sigla.
 *  Nel caso l'input sia nullo restituisce la stringa "XXX".
 */
void UDFEXPORT fn_cf_nome(const char str[], char *sigla)
{
  if (!sigla)
    return;

  sigla[0] = sigla[1] = sigla[2] = 'X';
  sigla[3] = 0;

  if (str)
  {
    char con[4], voc[3];
    unsigned ncon, nvoc, inserite = 0, i;

    cf_consonanti_vocali(str, con, &ncon, voc, &nvoc);

    /* Si considera, da prima, il caso particolare di nome costituito da piu'
       di tre consonanti... */
    if (ncon > 3)
    {
      sigla[0] = con[0];
      sigla[1] = con[2];
      sigla[2] = con[3];
    }
    else  /* ...e quindi il caso generale */
    {
      /* Si inseriscono tutte le consonanti sino ad un massimo di tre. */
      for (i = 0; inserite < 3 && i < ncon; ++i)
        sigla[inserite++] = (char)toupper(con[i]);

      /* Se c'e' spazio residuo, si inseriscono le vocali disponibili sino ad
         un massimo di tre. */
      for (i = 0; inserite < 3 && i < nvoc; ++i)
        sigla[inserite++] = (char)toupper(voc[i]);

      /* Se c'e' ancora spazio lo si riempie con delle X. */
      while (inserite < 3)
        sigla[inserite++] = 'X';
    }
  }
}

/**
 *  Restituisce 'M' o 'F' in base al sesso corrispondente al codice fiscale.
 */
void UDFEXPORT fn_cf_sesso(const char cf[], char *sesso)
{
  unsigned anno, mese, giorno;

  cf_nascita_sesso(cf, &anno, &mese, &giorno, sesso);
}
