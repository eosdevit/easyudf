/**
 *  \file
 *  \remark This file is part of EasyUDF.
 *
 *  \copyright Copyright (C) 2010-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(EASYUDF_STRING)
#define      EASYUDF_STRING

#include "easyudf.h"

void UDFEXPORT fn_floattostr(const NUMBER *const, const char [], char *);
INTEGER UDFEXPORT fn_ld(const char [], const char []);
SMALLINT UDFEXPORT fn_namepermutation(const char [], const char [], const char []);
void UDFEXPORT fn_soundex(const char [], char *);

#endif  /* include guard */
