# [EASY UDF][2] #

**This software is not developed anymore**
External functions (UDFs) have been aggressively deprecated in Firebird 4.0: replacement of UDFs with UDRs or stored functions is strongly recommended.

## Overview ##

EasyUDF is a set of powerful UDF functions that give users more ability to operate and manipulate data within Firebird. 

It's distributed under the [Mozilla Public License v2.0](https://www.mozilla.org/MPL/2.0/) (see the accompanying LICENSE file for more details).

This software was originally developed by [EOS][1] without open source in mind. Later (early 2017) the code has been commented, restructured, documented and released as open source.

## Build requirements ##

EasyUDF is designed to have fairly minimal requirements to build and use with your projects, but there are some. Currently, we support Linux and Windows.

If you notice any problems on your platform, please use the
[issue tracking system](https://bitbucket.org/eosdevit/easyudf/issues); patches for fixing them are even more welcome!

## Installing the library ##

### Windows ###

Copy the `EasyUDF.dll` file in `[Firebird path]\UDF` folder.

### Linux ###

* Set the appropriate owner/group/permissions for `EasyUDF.so` (usually `firebird/firebird/-rwxr-x---`).
* Copy the `EasyUDF.so` library into directory    `[Firebird path]/UDF/`


[1]: https://www.eosdev.it/
[2]: https://bitbucket.org/eosdevit/easyudf